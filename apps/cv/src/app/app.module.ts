import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { UiModule } from '@my/ui';
import { HostSettings } from '@my/core';
import { AuthModule, AuthHeaderInterceptor } from '@my/auth';
import { environment } from './../environments/environment';

export function hostSettingsFactory(): HostSettings {
  return {
    endpoint: environment.apiUrl
  };
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    AuthModule,
    UiModule
  ],
  providers: [
    {
      provide: HostSettings,
      useFactory: hostSettingsFactory,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthHeaderInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
