module.exports = {
  name: 'docs',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/docs',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
