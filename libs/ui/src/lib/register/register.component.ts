import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import  { AuthService } from '@my/auth';

@Component({
  selector: 'my-ui-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public form: FormGroup;
  private formSubmitAttempt: boolean = false;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService
  ) {
    this.form = this.fb.group({
      Name: ['', Validators.required],
      Password: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  isFieldInvalid(field: string) {
    let formField = this.form.get(field);
    return (
      (formField && !formField.valid && formField.touched) ||
      (formField && formField.untouched && this.formSubmitAttempt)
    );
  }

  onSubmit() {
    if (this.form.valid) {
      this.authService.register(this.form.value);
    }
    this.formSubmitAttempt = true;
  }

}
