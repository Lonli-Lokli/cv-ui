import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeLayoutComponent } from './layouts/home-layout.component';
import { LoginLayoutComponent } from './layouts/login-layout.component';
import { HeaderComponent } from './header/header.component';
import { ReactiveFormsModule } from '@angular/forms';
import { UiMaterialModule } from './material.module';
import { RouterModule } from '@angular/router';

const views = [
  LoginComponent,
  HomeLayoutComponent,
  LoginLayoutComponent,
  RegisterComponent,
  HeaderComponent
]
@NgModule({
  declarations: [
    ...views
  ],
  imports: [
    CommonModule,
    RouterModule,
    UiMaterialModule,
    ReactiveFormsModule
  ],
  exports: [
    ...views
  ]
})
export class UiModule {}
